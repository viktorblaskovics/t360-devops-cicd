const server = require('./dist/index.js');
const supertest = require('supertest');
const requestWithSupertest = supertest(server);


describe('Default endpoint', () => {

  it('GET / should return hello', async () => {
    const res = await requestWithSupertest.get('/');
      expect(res.status).toEqual(200);
      console.log(res.text)
      expect(res.text).toEqual('Hello CICD!');
      // expect(res.body).toHaveProperty('users')
  });

  afterAll(() => {
    server.close();
  });

});